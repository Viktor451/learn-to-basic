// Homework_14.3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <cstring>
using namespace std;


class Animal
{
public:
	virtual void Voice() = 0;

};

class Dog : public Animal
{
public:
	Dog() {}
	void Voice() override
	{
		std::cout << "Woof" << "\n";
	}

};

class Cat : public Animal
{
public:
	Cat() {}
	void Voice() 
	{
		std::cout << "Meow" << "\n";
	}
};
class Cow : public Animal
{
public:
	Cow() {}
	void Voice()
	{
		std::cout << "Moo" << "\n";
	}
};

int main()
{
	using std::cout;

	Animal* a = new Dog;
	Animal* b = new Cat;
	Animal* c = new Cow;

	a->Voice();
	b->Voice();
	c->Voice();



}

	

	
	

 

        
	 




// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
